# Cross-lingual annotation projection in legal texts

## License and credits

The content of this repository is available under GNU General Public License v3.0.

In case you use any of the resources in this repository, please cite this work as:

> Galassi A., Drazewski K., Lippi M., Torroni P., "Cross-lingual Annotation Projection in Legal Texts", Proceedings of the 28th International Conference on Computational Linguistics, Barcellona, 2020. DOI: 10.18653/v1/2020.coling-main.79

Our paper is available at the following URL:
<http://dx.doi.org/10.18653/v1/2020.coling-main.79/>

We have been invited to present and discuss our work at NLLP Talks #2, the recording is available here: <https://www.youtube.com/watch?v=fImtpIRKf4w>


## Corpus
Privacy Policy and Terms of Service documents, in German and English, annotated by an expert fluent in English and German


## Structure of the repository

### Corpus folder:
* original_text: the original textual documents
* original_xml: the annotated documents
* sentences: the processed documents, original and translated, with one sentence for each line
* tags: the annotation, with each line containing the tags of the corresponding sentence
* files containing the list of tags used in the documents and some statistics.

NOTATION:

* The subfolders *en* of the *sentences* folder contains the documents regarding sentences in english language, therefore the original English documents (original) and the German documents translated in English (translated). The same applies for the *de* subfolder and the *tags* folder.
* Similarly, in the *tags* folder, *projected-to-de* means that the known annotation is for the english document and the final target is the german document

### Resources folder:
Contains the pre-computed embeddings

### Evaluation folder:
Test of each dissimilarity measure on the two datasets (PP and ToS) and on both of them.

NOTATION: en-tde means that the tags on the english document have been projected to a german document translated in english

## Scripts

### translate.py
* translates a document with goslate (Google)
* prints translated version to standard output
* two arguments: (1) input file, (2) output language (two letters, e.g. 'de')
* example usage: python3 scripts/translate.py corpus/original_text/en/Dropbox.PP.txt
* === UPDATE: better to use the Apache Joshua standalone transalation tool! ===

### extract_tags_per_sentence.py
* extract tags for each sentence
* prints a line for each sentence, either empty or containing the list of tags for that sentence, separated with spaces
* three arguments: (1) xml input file, (2) list of tags to be considered (e.g., corpus/list_tags.txt), (3) sentences input file (one sentence per line)
* example usage: python3 scripts/extract_tags_per_sentence.py corpus/original_xml/en/Dropbox.PP.xml corpus/list_tags.txt corpus/sentences/en/original/PP/Dropbox.PP.txt


### project_tags.py
* projection of tags from one document into another
* outputs list of tags, one line per sentence
* compute distance with custom function (for the moment, using the textdistance library)
* four arguments: (1) source document (one sentence per line); (2) target document to be tagged (one sentence per line); (3) tags of source document (one sentence per line); (4) int representing distance to be used (more about this below)
* example usage, to project from the original English to the (translated) German using Hamming distance: python3 scripts/project_tags.py corpus/sentences/en/original/PP/Dropbox.PP.txt corpus/sentences/en/translated/PP/Dropbox.PP.txt corpus/tags/en/original/PP/Dropbox.PP.txt -d 1

### project_tags_to_xml.py
* same as above, but write output as xml file

### elmo_project_tags.py
* similar to project_tags, but makes use of elmo embeddings
* it is possible to load pre-computed embeddings providing the path of the embeddings file (option -e), and update such embeddings in case of new sentences (option -u)
* pre-computed embeddings are available in the "resources"

### evaluate_projection.py
* computes many metrics for a projected tag document
* 3 arguments: (1) ground truth file; (2) prediction file to be evaluated; (3) list of tags to be considered (e.g., corpus/list_tags.txt)
* can evaluate an entire folder (option -d)
* use options to select the desired metric
* example usage, to evaluate the projection from English to German: python scripts/evaluate_projection.py corpus/tags/de/original/PP/Dropbox.PP.txt corpus/tags/en/projected-to-de/PP/1/Dropbox.PP.txt corpus/list_tags_PP.txt -aiwprf

### show_errors.py
* highlights the projection errors
* 4 arguments: (1) ground truth file; (2) prediction file to be evaluated; (3) list of tags to be considered (e.g., corpus/list_tags.txt); (4) sentences file
* can evaluate an entire folder (option -d)
* use verbose option (-v) to print more information
* example usage, to evaluate the projection from English to German: python scripts/evaluate_projection.py corpus/tags/de/original/PP/Dropbox.PP.txt corpus/tags/en/projected-to-de/PP/1/Dropbox.PP.txt corpus/list_tags_PP.txt corpus/sentences/de/original/PP/Dropbox.PP.txt



### .bash and .sh files
* utility scripts

### Numerical code and corresponding approaches
* 1 : hamming (text)
* 2 : levenshtein (text)
* 3 : damerau levenshtein (text)
* 4 : needleman wunsch (text)
* 6 : jaccard (text)
* 10 : entropy ncd (text)
* 101 : hamming (text) + DTW
* 102 : levenshtein (text) + DTW
* 103 : damerau levenshtein (text) + DTW
* 104 : needleman wunsch (text) + DTW
* 106 : jaccard (text) + DTW
* 110 : entropy ncd (text) + DTW
* 12 : cosine (emb) + normalization
* 42 : bray curtis (emb) + normalization
* 16 : cosine (emb) + DTW + normalization
* 46 : bray curtis (emb) + DTW + normalization