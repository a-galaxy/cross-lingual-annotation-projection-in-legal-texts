# example usage: python scripts/show_errors.py
#       corpus/tags/de/original/PP/Dropbox.PP.txt
#       corpus/tags/en/projected-to-de/PP/1/Dropbox.PP.txt
#       corpus/list_tags_PP.txt
#       corpus/sentences/de/original/PP/Dropbox.PP.txt


import sys
import os
import argparse
import numpy as np
from sklearn.metrics import f1_score, precision_recall_fscore_support, classification_report


# from sklearn.metrics import multilabel_confusion_matrix


def estract_tag_list_from_file(tag_list, document_path):
    """
    Estracts the tags from a document. Return a list multi-label representation of the document.
    Each element in the list represents a line.
    Each line element is a list that contains a set of 0 and 1 values, one for each possible label.
    1 indicates the presence of a specific tag in that line. 0 indicates the absence of that tag.
    :param tag_list: list of tags that can be present in the document
    :param document_path: path of the document to be analyzed. Only tags are considered.
    :return: A list that contains the multi-label representation of the document
    """
    # for evaluation, the structure that is needed is the following:
    # array = [ [0, 1, 1, ...] , [0, 1, 0, ...] ]
    # each array element in the array represent a line
    # each element in the nested array represent the presence (1) or absence (0) of a given tag
    document_data = []
    with open(document_path) as f:
        for line in f:
            line_data = []
            labels = line.strip().lower().split()
            for tag in tag_list:
                if tag in labels:
                    line_data.append(tag)
                else:
                    line_data.append("NOT_" + tag)
            document_data.append(line_data)
    return document_data


parser = argparse.ArgumentParser(description="Show errors of tag projection for a document or"
                                             "for all the documents in a folder")
parser.add_argument("ground_truth_path", help="The path that contains the ground truth")
parser.add_argument("prediction_path", help="The path that will be evaluated")
parser.add_argument("tags_list_document", help="The path of the document with the list of tags")
parser.add_argument("sentences_path", help="The path that contains the sentences file")
parser.add_argument('-d', '--directory', help="Operates on an entire folder instead of a single document",
                    action="store_true")
parser.add_argument('-v', '--verbose', help="Prints every sentence and the full list of tags",
                    action="store_true")

args = parser.parse_args()

ground_truth_tags_document = args.ground_truth_path
projected_tags_document = args.prediction_path
sentences_document = args.sentences_path
tag_list_document = args.tags_list_document
verbose = args.verbose

tag_list = []

# extract possible tag list
with open(tag_list_document) as f:
    for line in f:
        tag_list.append(line.strip().lower())
tag_list.sort()


gt_documents = []
p_documents = []
s_documents = []

if not args.directory:
    if os.path.isfile(ground_truth_tags_document) and os.path.isfile(projected_tags_document)\
            and os.path.isfile(sentences_document):
        gt_documents = [ground_truth_tags_document]
        p_documents = [projected_tags_document]
        s_documents = [sentences_document]
    else:
        print('ILLEGAL PATHS:\n\t' + ground_truth_tags_document + '\n\t' + projected_tags_document +
              '\n\t' + sentences_document)
        print('Aborting...')
        sys.exit()
else:
    # if a folder is selected
    if os.path.isdir(projected_tags_document) and os.path.isdir(ground_truth_tags_document)\
            and os.path.isdir(sentences_document):
        p_documents_names = os.listdir(projected_tags_document)
        # for each file in the prediction folder
        for document_name in p_documents_names:
            # check if a corresponding ground truth file does exist
            gt_document = os.path.join(ground_truth_tags_document, document_name)
            p_document = os.path.join(projected_tags_document, document_name)
            s_document = os.path.join(sentences_document, document_name)
            if os.path.isfile(gt_document) and os.path.isfile(p_document) and os.path.isfile(s_document):
                gt_documents.append(gt_document)
                p_documents.append(p_document)
                s_documents.append(s_document)
            else:
                print('ILLEGAL PATH:\n\t' + gt_document + '\n\t' + p_document + '\n\t' + s_document)
                print('Skipping...')
    else:
        print('ILLEGAL PATHS:\n\t' + ground_truth_tags_document + '\n\t' + projected_tags_document +
              '\n\t' + sentences_document)
        print('Aborting...')
        sys.exit()


real_tag_list = []
for gt_tag_document in gt_documents:
    # extract tags in the document
    with open(gt_tag_document) as f:
        tags = f.read().split()
        for tag in tags:
            if tag in tag_list and tag not in real_tag_list:
                real_tag_list.append(tag)

real_tag_list.sort()


# dimension 1 = files
# dimension 2 = sentences
# dimension 3 = tags
prediction = []
ground_truth = []


for gt_document in gt_documents:
    document_ground_truth = estract_tag_list_from_file(real_tag_list, gt_document)
    ground_truth.append(document_ground_truth)
for p_document in p_documents:
    document_prediction = estract_tag_list_from_file(real_tag_list, p_document)
    prediction.append(document_prediction)

# dimension 1 = files
# dimension 2 = sentences
sentences = []
for s_document in s_documents:
    with open(s_document) as f:
        sentences.append(f.readlines())

for d_idx in range(len(prediction)):
    print("Document:\t" + gt_documents[d_idx])
    print()
    d_pred = prediction[d_idx]
    d_truth = ground_truth[d_idx]
    d_sent = sentences[d_idx]

    for s_idx in range(len(d_pred)):
        s_pred = d_pred[s_idx]
        s_truth = d_truth[s_idx]
        s_sent = d_sent[s_idx]

        fp = []
        fn = []
        for t_idx in range(len(s_pred)):
            t_pred = s_pred[t_idx]
            t_truth = s_truth[t_idx]
            if t_pred != t_truth:
                if "NOT_" in t_truth:
                    fp.append(t_pred)
                elif "NOT_" in t_pred:
                    fn.append(t_truth)
                else:
                    print("ERROR ON TAGS")
                    print(s_sent)
                    print(s_truth)
                    print(s_pred)
                    exit(-4)
        if verbose or len(fp) > 0 or len(fn) > 0:
            print("S " + str(s_idx+1) + "\t" + s_sent)
            if verbose:
                print("\tTruth:\t" + str(s_truth))
                print("\tProj:\t" + str(s_pred))
            out_str = ""
            out_str += "\t\tFP:\t"
            for tag in fp:
                out_str += tag + "\t"
            print(out_str)

            out_str = ""
            out_str += "\t\tFN:\t"
            for tag in fn:
                out_str += tag + "\t"
            print(out_str)
            print("\t-------------------------------------")
    print()
    print("========================================================================================================")
    print("========================================================================================================")
    print("========================================================================================================")
    print()


