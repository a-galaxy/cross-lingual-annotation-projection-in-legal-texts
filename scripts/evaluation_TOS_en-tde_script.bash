#!/usr/bin/env bash

# perform the evaluation of a list of documents for each distance

for i in  1 # 2 3 4 6 10
do
	echo "i: "$i

	for filename in "Garmin.TOS" "Box.TOS" "Grindr.TOS" "Linkedin.TOS" "MyHeritage.TOS"
	do
		echo "f: "$filename
		filefolder="../corpus/tags/en/projected-to-de/TOS/"$i
		filepath="../corpus/tags/en/projected-to-de/TOS/"$i"/"$filename".txt"
		evaluation_file="../evaluation/"$filename".en-tde."$i".txt"
		python3.5 ./evaluate_projection.py "../corpus/tags/de/original/TOS/"$filename".txt" $filepath "../corpus/list_tags_TOS.txt" -aiwprf > $evaluation_file

		echo "finished: "$filename
	done

	filefolder="../corpus/tags/en/projected-to-de/TOS/"
	filepath="../corpus/tags/en/projected-to-de/TOS/"$i"/"
	evaluation_file="../evaluation/ALL.TOS.en-tde."$i".txt"
	python3.5 ./evaluate_projection.py "../corpus/tags/de/original/TOS/" $filepath "../corpus/list_tags_TOS.txt" -aiwprfd > $evaluation_file

	echo "finished: "$i
done
