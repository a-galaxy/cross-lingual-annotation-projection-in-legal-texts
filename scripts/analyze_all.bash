#!/bin/bash


evaluation_file="../corpus/stats-en.txt"
python3.5 ./analyze_stats.py "../corpus/tags/en/original/BOTH/" "../corpus/list_tags.txt" -dt > $evaluation_file
echo "" >> $evaluation_file
echo "" >> $evaluation_file
echo "" >> $evaluation_file
echo "" >> $evaluation_file


echo "TOS" >> $evaluation_file
python3.5 ./analyze_stats.py "../corpus/tags/en/original/TOS/" "../corpus/list_tags.txt" -d >> $evaluation_file
echo "" >> $evaluation_file

for filename in "Garmin.TOS" "Box.TOS" "Grindr.TOS" "Linkedin.TOS" "MyHeritage.TOS"
do
	echo $filename  >> $evaluation_file
	filepath="../corpus/tags/en/original/BOTH/"$filename".txt"
	python3.5 ./analyze_stats.py $filepath "../corpus/list_tags.txt" >> $evaluation_file
	echo "" >> $evaluation_file
done

echo "" >> $evaluation_file
echo "" >> $evaluation_file
echo "" >> $evaluation_file

echo "PP" >> $evaluation_file
python3.5 ./analyze_stats.py "../corpus/tags/en/original/PP/" "../corpus/list_tags.txt" -d >> $evaluation_file
echo "" >> $evaluation_file

for filename in "Dropbox.PP" "Facebook.PP" "Supercell.PP" "Tumblr.PP" "Twitter.PP"
do
	echo $filename  >> $evaluation_file
	filepath="../corpus/tags/en/original/BOTH/"$filename".txt"
	python3.5 ./analyze_stats.py $filepath "../corpus/list_tags.txt" >> $evaluation_file
	echo "" >> $evaluation_file
done


evaluation_file="../corpus/stats-de.txt"
python3.5 ./analyze_stats.py "../corpus/tags/de/original/BOTH/" "../corpus/list_tags.txt" -dt > $evaluation_file
echo "" >> $evaluation_file
echo "" >> $evaluation_file
echo "" >> $evaluation_file
echo "" >> $evaluation_file


echo "TOS" >> $evaluation_file
python3.5 ./analyze_stats.py "../corpus/tags/de/original/TOS/" "../corpus/list_tags.txt" -d >> $evaluation_file
echo "" >> $evaluation_file

for filename in "Garmin.TOS" "Box.TOS" "Grindr.TOS" "Linkedin.TOS" "MyHeritage.TOS"
do
	echo $filename  >> $evaluation_file
	filepath="../corpus/tags/de/original/BOTH/"$filename".txt"
	python3.5 ./analyze_stats.py $filepath "../corpus/list_tags.txt" >> $evaluation_file
	echo "" >> $evaluation_file
done

echo "" >> $evaluation_file
echo "" >> $evaluation_file
echo "" >> $evaluation_file

echo "PP" >> $evaluation_file
python3.5 ./analyze_stats.py "../corpus/tags/de/original/PP/" "../corpus/list_tags.txt" -d >> $evaluation_file
echo "" >> $evaluation_file

for filename in "Dropbox.PP" "Facebook.PP" "Supercell.PP" "Tumblr.PP" "Twitter.PP"
do
	echo $filename  >> $evaluation_file
	filepath="../corpus/tags/de/original/BOTH/"$filename".txt"
	python3.5 ./analyze_stats.py $filepath "../corpus/list_tags.txt" >> $evaluation_file
	echo "" >> $evaluation_file
done