#!/usr/bin/env bash

for i in 1 2 3 4 6 10 101 102 103 104 106 110
do
	for filename in "Dropbox.PP.txt" "Facebook.PP.txt" "Supercell.PP.txt" "Tumblr.PP.txt" "Twitter.PP.txt"
	do
		echo "i: "$i
		filefolder="../corpus/tags/en/projected-to-de/PP/"$i
		filepath="../corpus/tags/en/projected-to-de/PP/"$i"/"$filename
		mkdir -p $filefolder
		echo $filepath
		python3.5 ./project_tags.py "../corpus/sentences/en/original/PP/"$filename "../corpus/sentences/en/translated/PP/"$filename "../corpus/tags/en/original/PP/"$filename -d $i > $filepath 2> "../logs/err.en-tde."$i"."$filename &
		echo "finished"
	done
done
