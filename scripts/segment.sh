input_dir=$1
output_dir=$2
language=$3

for input_file in `ls $input_dir`
do

if test $language == "en"
then
	java -cp "stanford-corenlp-full-2017-06-09/*:stanford-parse-models-1.3.5.jar:." edu.stanford.nlp.parser.lexparser.LexicalizedParser -outputFormat "words" edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz $input_dir/$input_file | grep . > $output_dir/$input_file
else
	java -cp "stanford-corenlp-full-2017-06-09/*:stanford-german-corenlp-2018-10-05-models.jar:." edu.stanford.nlp.parser.lexparser.LexicalizedParser -outputFormat "words" edu/stanford/nlp/models/lexparser/germanPCFG.ser.gz $input_dir/$input_file | grep . > $output_dir/$input_file
fi

done

