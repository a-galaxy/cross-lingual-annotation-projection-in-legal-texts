import goslate
import sys

data_file = sys.argv[1]
language = sys.argv[2]

with open(data_file, 'r') as f:
    text = f.read()

gs = goslate.Goslate()
print(gs.translate(text, language))


