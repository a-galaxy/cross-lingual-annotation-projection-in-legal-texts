input_dir=$1
output_dir=$2

for input_file in `ls $input_dir`
do

output_file=`echo $input_file | sed 's/\.xml/\.txt/g'`
cat $input_dir/$input_file | sed 's/<[a-z]*[1-4]>//g;s/<\/[a-z]*[1-4]>//g;s/<out>//g;s/<\/out>//g;s/<vag>//g;s/<\/vag>//g' > $output_dir/$output_file

done

