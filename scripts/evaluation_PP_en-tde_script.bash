#!/usr/bin/env bash

# perform the evaluation of a list of documents for each distance

for i in  1 2 3 4 6 10 101 102 103 104 106 110
do
	echo "i: "$i

	for filename in "Dropbox.PP" "Facebook.PP" "Supercell.PP" "Tumblr.PP" "Twitter.PP"
	do
		echo "f: "$filename
		filefolder="../corpus/tags/en/projected-to-de/PP/"$i
		filepath="../corpus/tags/en/projected-to-de/PP/"$i"/"$filename".txt"
		evaluation_file="../evaluation/"$filename".en-tde."$i".txt"
		python3.5 ./evaluate_projection.py "../corpus/tags/de/original/PP/"$filename".txt" $filepath "../corpus/list_tags_PP.txt" -aiwprf > $evaluation_file

		echo "finished: "$filename
	done

	filefolder="../corpus/tags/en/projected-to-de/PP/"
	filepath="../corpus/tags/en/projected-to-de/PP/"$i"/"
	evaluation_file="../evaluation/ALL.PP.en-tde."$i".txt"
	python3.5 ./evaluate_projection.py "../corpus/tags/de/original/PP/" $filepath "../corpus/list_tags_PP.txt" -aiwprfd > $evaluation_file

	echo "finished: "$i
done
