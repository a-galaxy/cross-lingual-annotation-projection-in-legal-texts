#!/usr/bin/env bash

# perform the evaluation of a list of documents for each distance

for i in  1 2 3 4 6 10 101 102 103 104 106 110
do
	echo "i: "$i

	filefolder="../corpus/tags/de/projected-to-de/BOTH/"
	filepath="../corpus/tags/de/projected-to-de/BOTH/"$i"/"
	evaluation_file="../evaluation/ALL.BOTH.ten-de."$i".txt"
	python3.5 ./evaluate_projection.py "../corpus/tags/de/original/BOTH/" $filepath "../corpus/list_tags.txt" -aiwprfd > $evaluation_file

	echo "finished: "$i
done
