import sys
import os
import argparse

parser = argparse.ArgumentParser(description="Analyze a document or a folder, providing tags information")
parser.add_argument("file_path", help="The path that of the document or the folder")
parser.add_argument("tags_list_document", help="The path of the document with the list of tags")
parser.add_argument('-d', '--directory', help="Operates on an entire folder instead of a single document",
                    action="store_true")
parser.add_argument('-t', '--top_header', help="Prints also the header",
                    action="store_true")

args = parser.parse_args()

analyzed_document = args.file_path
tag_list_document = args.tags_list_document

tag_list = []

# extract possible tag list
with open(tag_list_document) as f:
    for line in f:
        tag_list.append(line.strip().lower())
tag_list.sort()

tag_dictionary = dict()
sentences = 0
n_tags = 0

for tag in tag_list:
    tag_dictionary[tag] = 0

p_documents = []

if not args.directory:
    if os.path.isfile(analyzed_document):
        p_documents = [analyzed_document]
    else:
        print('ILLEGAL PATHS:\n\t' + args.ground_truth_path + '\n\t' + args.prediction_path)
        print('Aborting...')
        sys.exit()
else:
    # if a folder is selected
    if os.path.isdir(analyzed_document):
        p_documents_names = os.listdir(analyzed_document)
        # for each file in the prediction folder
        for document_name in p_documents_names:
            # check if a corresponding ground truth file does exist
            p_document = os.path.join(analyzed_document, document_name)
            if os.path.isfile(p_document):
                p_documents.append(p_document)
            else:
                print('ILLEGAL PATH:\n\t' + p_document)
                print('Skipping...')
    else:
        print('ILLEGAL PATHS:\n\t' + analyzed_document)
        print('Aborting...')
        sys.exit()

for document in p_documents:
    # extract tags in the document
    with open(document) as f:
        lines = f.readlines()
        sentences += len(lines)
        doc = " ".join(lines)
        tags = doc.split()
        n_tags += len(tags)
        for tag in tags:
            if tag in tag_list:
                tag_dictionary[tag] += 1

out = ""

if args.top_header:
    out += "Sentences\tTags"
    for tag in tag_list:
        out += "\t" + str(tag)
    out += "\n"

out += str(sentences) + "\t" + str(n_tags)
for tag in tag_list:
    out += "\t" + str(tag_dictionary[tag])

print(out)