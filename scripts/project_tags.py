import textdistance
import sys
import argparse
from tqdm import tqdm
from fastdtw import fastdtw
from collections import defaultdict

parser = argparse.ArgumentParser(description="Project tags from a source textual document, "
                                             "in which the labels are specified, to another textual document, "
                                             "for which the tags are unknown. "
                                             "The matches and the distances are printed on stderr.")
parser.add_argument("source_text", help="The path of the sentences document for which the tags are known")
parser.add_argument("target_text", help="The path of the sentences document for which the tags are unknown")
parser.add_argument("source_tags", help="The path of the tags document related to the first sentence document")

parser.add_argument('-d', '--distance', type=int,
                    choices=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
                             101, 102, 103, 104, 105, 106, 107, 108, 109, 110, ],
                    help="distance used for matching", default="1")

args = parser.parse_args()

document_1 = args.source_text  # Source: tags known
document_2 = args.target_text  # Target: to be tagged
tags_document_1 = args.source_tags  # Tags of document_1
which_distance = int(args.distance)

# TODO: check distance 9, it seems to have problems: no match done

#  DISTANCES 
#  1- hamming (edit based)
#  2- levenshtein (edit based)
#  3- damerau_levenshtein (edit based)
#  4- needleman_wunsch (edit based)
#  5- smith_waterman (edit based)
#  6- jaccard (token based)
#  7- tanimoto (token based)
#  8- cosine (token based)
#  9- rle_ncd (compression based)
# 10- entropy_ncd (compression based)
# 100 + distance = DTW using that distance

dtw_distances = [101, 102, 103, 104, 105, 106, 107, 108, 109, 110, ]

if which_distance == 1 or which_distance == 101:
    distance_computer = textdistance.hamming
elif which_distance == 2 or which_distance == 102:
    distance_computer = textdistance.levenshtein
elif which_distance == 3 or which_distance == 103:
    distance_computer = textdistance.damerau_levenshtein
elif which_distance == 4 or which_distance == 104:
    distance_computer = textdistance.needleman_wunsch
elif which_distance == 5 or which_distance == 105:
    distance_computer = textdistance.smith_waterman
elif which_distance == 6 or which_distance == 106:
    distance_computer = textdistance.jaccard
elif which_distance == 7 or which_distance == 107:
    distance_computer = textdistance.tanimoto
elif which_distance == 8 or which_distance == 108:
    distance_computer = textdistance.cosine
elif which_distance == 9 or which_distance == 109:
    distance_computer = textdistance.rle_ncd
elif which_distance == 10 or which_distance == 110:
    distance_computer = textdistance.entropy_ncd
else:
    print('Unknown distance value: ' + str(which_distance))
    sys.exit()

sentences_document_tagged = []
sentences_document_to_be_tagged = []
original_sentences_document_to_be_tagged = []
labels_document_tagged = []

with open(document_1) as f:
    for line in f:
        sentences_document_tagged.append(line.strip().lower())

with open(document_2) as f:
    for line in f:
        sentences_document_to_be_tagged.append(line.strip().lower())
        original_sentences_document_to_be_tagged.append(line.strip())

with open(tags_document_1) as f:
    for line in f:
        labels_document_tagged.append(line.strip())


def dtw(x, y, window, dist):
    """
    Based on fastdtw.dtw
    return the distance between 2 time series without approximation

    Parameters
    ----------
    x : array_like
        input array 1
    y : array_like
        input array 2
    dist : function or int
        The method for calculating the distance between x[i] and y[j]. If
        dist is an int of value p > 0, then the p-norm will be used. If
        dist is a function then dist(x[i], y[j]) will be used. If dist is
        None then abs(x[i] - y[j]) will be used.

    Returns
    -------
    distance : float
        the approximate distance between the 2 time series
    path : list
        list of indexes for the inputs x and y
    """
    len_x, len_y = len(x), len(y)
    if window is None:
        window = [(i, j) for i in range(len_x) for j in range(len_y)]
    window = ((i + 1, j + 1) for i, j in window)
    D = defaultdict(lambda: (float('inf'),))
    D[0, 0] = (0, 0, 0)
    for i, j in tqdm(window):
        dt = dist(x[i - 1], y[j - 1])
        D[i, j] = min((D[i - 1, j][0] + dt, i - 1, j), (D[i, j - 1][0] + dt, i, j - 1),
                      (D[i - 1, j - 1][0] + dt, i - 1, j - 1), key=lambda a: a[0])
    path = []
    i, j = len_x, len_y
    while not (i == j == 0):
        path.append((i - 1, j - 1))
        i, j = D[i, j][1], D[i, j][2]
    path.reverse()
    return (D[len_x, len_y][0], path)


matches = dict()
total_distance = 0
# USE OF DTW ALGORITHM
if which_distance in dtw_distances:
    total_distance, path = dtw(sentences_document_to_be_tagged, sentences_document_tagged,
                               dist=distance_computer, window=None)
    # print(path) # DEBUG

    prev_idx_1 = 0
    out = ""
    for couple in tqdm(path):
        idx_1 = couple[0]
        idx_2 = couple[1]
        if prev_idx_1 != idx_1:  # if the sentence to be tagged has changed, print the labels of the previous one
            print(out)
            out = ""
        prev_idx_1 = idx_1


        couple_distance = distance_computer.distance(sentences_document_to_be_tagged[idx_1],
                                                     sentences_document_tagged[idx_2])
        matches[(couple)]=couple_distance

        # tag = doc1_data[idx_2] # DEBUG
        # out += tag + " --- " # DEBUG
        for tag in labels_document_tagged[idx_2].split(' '):
            if tag != '':
                out += tag + ' '
    print(out)
else:
    idx_1 = 0

    number_of_sentences = len(sentences_document_to_be_tagged)
    number_of_outliers = number_of_sentences/20

    # 1 to N procedure: each sentence to be tagged is matched with 1 tagged sentence
    for s_to_be_tagged in tqdm(sentences_document_to_be_tagged):
        min_dist = 1e30
        idx_2 = 0
        for s_tagged in sentences_document_tagged:
            d = distance_computer.distance(s_to_be_tagged, s_tagged)
            if d < min_dist:
                min_dist = d
                idx_2_best = idx_2
            idx_2 = idx_2 + 1
        out = ''
        total_distance += min_dist
        for tag in labels_document_tagged[idx_2_best].split(' '):
            if tag != '':
                out += tag + ' '
        print(out)
        matches[(idx_1, idx_2_best)]=min_dist
        idx_1 = idx_1 + 1

for key in matches.keys():
    print(str(key) + "\t" + str(matches[key]), file=sys.stderr)
