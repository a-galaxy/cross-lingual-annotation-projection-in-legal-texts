input_file=$1
lang_in=$2
lang_out=$3

while read line
do
	translate -s $lang_in -d $lang_out "`echo $line`"

done < $input_file | grep -E "^\[$lang_out\]" | cut -d' ' -f2-

