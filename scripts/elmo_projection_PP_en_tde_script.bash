#!/bin/bash

# perform the evaluation of a list of documents for each distance

for i in 11 12 15 16 41 42 45 46
do
	for filename in "Dropbox.PP.txt" "Facebook.PP.txt" "Supercell.PP.txt" "Tumblr.PP.txt" "Twitter.PP.txt"
	do
		echo "i: "$i
		filefolder="../corpus/tags/en/projected-to-de/PP/"$i
		filepath="../corpus/tags/en/projected-to-de/PP/"$i"/"$filename
		mkdir -p $filefolder
		echo $filepath
		python3.5 ./elmo_project_tags.py "../corpus/sentences/en/original/PP/"$filename "../corpus/sentences/en/translated/PP/"$filename "../corpus/tags/en/original/PP/"$filename -d $i -e "../resources/elmo_embeddings_en.npy" -u -n > $filepath 2> "../logs/err.en-tde."$i"."$filename
		echo "finished"
	done
done
