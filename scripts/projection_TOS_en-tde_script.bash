#!/usr/bin/env bash

for i in 1 2 3 4 6 10 101 102 103 104 106 110
do
	for filename in  "Garmin.TOS.txt" "Box.TOS.txt" "Grindr.TOS.txt" "Linkedin.TOS.txt" "MyHeritage.TOS.txt"
    do
		echo "i: "$i
		filefolder="../corpus/tags/en/projected-to-de/TOS/"$i
		filepath="../corpus/tags/en/projected-to-de/TOS/"$i"/"$filename
		mkdir -p $filefolder
		echo $filepath
		python3.5 ./project_tags.py "../corpus/sentences/en/original/TOS/"$filename "../corpus/sentences/en/translated/TOS/"$filename "../corpus/tags/en/original/TOS/"$filename -d $i > $filepath 2> "../logs/err.en-tde."$i"."$filename &
		echo "finished"
	done
done
