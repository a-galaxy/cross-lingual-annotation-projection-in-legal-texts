from bs4 import BeautifulSoup
import string
import sys

data_file = sys.argv[1]
tag_list_file = sys.argv[2]
sentence_list_file = sys.argv[3]

xml = []
tags = []
sentences = []

with open(data_file) as f:
    for line in f:
        xml.append(line.strip())

with open(tag_list_file) as f:
    for line in f:
        tags.append(line.strip())

with open(sentence_list_file) as f:
    for line in f:
        sentences.append(line.strip().replace("-lrb-","(").replace("-rrb-",")").replace("-lsb-","[").replace("-rsb-","]").replace("-LRB-","(").replace("-RRB-",")").replace("-LSB-","[").replace("-RSB-","]"))

soup = BeautifulSoup(''.join(xml), "html.parser")

positives = []

for tag in tags:
    res = soup.find_all(tag)
    for r in res:
        positives.append([tag,r.text,0])

sentence_tags = []

for sentence in sentences:
    s_tags = []
    label = -1
    s = ''.join(ch for ch in sentence if ch.isalpha()).lower()
    for ch in string.punctuation:
        sentence = sentence.replace(ch, "")
    num_words = len(sentence.strip().split())
    count = 0
    for p in positives:
        tag = p[0]
        text = p[1]
        target = ''.join(ch for ch in text if ch.isalpha()).lower()
        if target in s:
            if tag not in s_tags:
                s_tags.append(tag)
        if s in target and num_words > 6:
            if tag not in s_tags:
                s_tags.append(tag)
            p[2] = 1
        count = count + 1
    out = ''
    for tag in s_tags:
        out += tag + ' '
    #out += '\n'
    print(out)

#for j in range(len(positives)):
#    if positives[j][2] == 0:
#        print("ERROR " + str(positives[j]))




