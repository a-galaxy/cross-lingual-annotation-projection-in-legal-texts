F1-macro	0.88
F1-micro	0.88
F1-weighted	0.88
Precision	0.96
Recall	0.81
              precision    recall  f1-score   support

         ad1       1.00      0.67      0.80         3
         ad2       1.00      0.75      0.86        12
         ad3       1.00      1.00      1.00        15
      basis1       1.00      0.85      0.92        33
      basis2       1.00      0.86      0.93        22
        cat1       0.91      0.76      0.83        38
        cat2       0.93      0.78      0.85        89
     source1       1.00      0.73      0.84        22
     source2       1.00      0.87      0.93        15
         ta1       1.00      0.67      0.80         6
         ta3       0.50      1.00      0.67         1
         tc1       1.00      0.88      0.93         8
         tc2       0.90      0.86      0.88        22
         tc3       0.94      0.85      0.89        20
        tpr1       1.00      0.78      0.88         9
        tpr2       1.00      0.67      0.80        12
         tu1       1.00      0.95      0.98        21
         tu3       1.00      1.00      1.00         3

   micro avg       0.96      0.81      0.88       351
   macro avg       0.95      0.83      0.88       351
weighted avg       0.96      0.81      0.88       351
 samples avg       0.26      0.26      0.26       351

