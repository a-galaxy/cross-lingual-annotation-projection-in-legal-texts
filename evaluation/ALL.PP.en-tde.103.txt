F1-macro	0.85
F1-micro	0.84
F1-weighted	0.84
Precision	0.93
Recall	0.77
              precision    recall  f1-score   support

         ad1       1.00      0.67      0.80         3
         ad2       1.00      0.75      0.86        12
         ad3       0.94      1.00      0.97        15
      basis1       0.97      0.85      0.90        33
      basis2       1.00      0.86      0.93        22
        cat1       0.93      0.71      0.81        38
        cat2       0.91      0.79      0.84        89
     source1       0.94      0.73      0.82        22
     source2       0.87      0.87      0.87        15
         ta1       1.00      0.67      0.80         6
         ta3       1.00      1.00      1.00         1
         tc1       0.86      0.75      0.80         8
         tc2       0.94      0.68      0.79        22
         tc3       0.75      0.75      0.75        20
        tpr1       1.00      0.78      0.88         9
        tpr2       0.86      0.50      0.63        12
         tu1       1.00      0.76      0.86        21
         tu3       1.00      1.00      1.00         3

   micro avg       0.93      0.77      0.84       351
   macro avg       0.94      0.78      0.85       351
weighted avg       0.93      0.77      0.84       351
 samples avg       0.25      0.25      0.25       351

