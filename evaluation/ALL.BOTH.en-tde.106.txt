F1-macro	0.78
F1-micro	0.83
F1-weighted	0.83
Precision	0.89
Recall	0.77
              precision    recall  f1-score   support

          a1       0.00      0.00      0.00         1
          a2       1.00      0.67      0.80         6
          a3       1.00      0.67      0.80         3
         ad1       1.00      0.67      0.80         3
         ad2       1.00      0.75      0.86        12
         ad3       1.00      1.00      1.00        15
      basis1       0.97      0.85      0.90        33
      basis2       1.00      0.86      0.93        22
        cat1       0.90      0.68      0.78        38
        cat2       0.86      0.79      0.82        89
         ch2       0.91      0.86      0.89        37
         cr2       1.00      0.57      0.73         7
         cr3       0.93      0.81      0.87        16
          j1       1.00      0.67      0.80         3
          j3       0.69      0.92      0.79        12
        law1       1.00      1.00      1.00         4
        law2       0.82      0.82      0.82        11
        ltd1       0.50      1.00      0.67         1
        ltd2       0.93      0.66      0.77        38
        ltd3       1.00      0.33      0.50         3
       pinc2       0.20      0.50      0.29         2
     source1       0.89      0.73      0.80        22
     source2       0.87      0.87      0.87        15
         ta1       1.00      0.67      0.80         6
         ta3       1.00      1.00      1.00         1
         tc1       0.86      0.75      0.80         8
         tc2       0.94      0.73      0.82        22
         tc3       0.76      0.80      0.78        20
        ter2       0.75      0.80      0.77        15
        ter3       0.93      0.76      0.84        17
        tpr1       1.00      0.78      0.88         9
        tpr2       0.86      0.50      0.63        12
         tu1       1.00      0.86      0.92        21
         tu3       1.00      1.00      1.00         3
        use2       0.75      0.69      0.72        13

   micro avg       0.89      0.77      0.83       540
   macro avg       0.87      0.74      0.78       540
weighted avg       0.90      0.77      0.83       540
 samples avg       0.15      0.15      0.15       540

