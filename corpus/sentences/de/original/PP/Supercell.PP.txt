DATENSCHUTZ DEUTSCH TRUSTe Datum des Inkrafttretens : 25 .
Mai 2018 Danke , dass Sie unsere Spiele gewählt haben !
Inhalt der vorliegenden Datenschutzerklärung : Wie und warum wir Ihre personenbezogenen Daten erfassen Wozu wir Ihre personenbezogenen Daten verwenden und Ihre Wahlmöglichkeiten für die Behandlung Ihrer personenbezogenen Daten .
Diese Datenschutzrichtlinie gilt für Spiele , Websites und zugehörige Dienste von Supercell , die wir hier mit dem Sammelbegriff `` Dienste '' bezeichnen .
Diese Datenschutzrichtlinie kann regelmäßig von uns aktualisiert werden , indem wir eine neue Version auf supercell.com veröffentlichen .
Bei wesentlichen Änderungen dieser Richtlinie werden wir Sie durch einen Hinweis innerhalb des Dienstes davon in Kenntnis setzen , bevor diese Änderungen wirksam werden .
Ihre weitere Nutzung des Dienstes nach dem Datum des Inkrafttretens unterliegt der neuen Datenschutzrichtlinie .
KONTAKTAUFNAHME Wenn Sie Fragen zum Datenschutz oder Wünsche hinsichtlich der Lösung von Problemen mit Ihren personenbezogenen Daten haben , bitten wir Sie , uns in erster Linie über das Spiel zu kontaktieren , damit wir Ihnen schneller antworten können .
Name des für die Daten Verantwortlichen : Supercell Oy Adresse : Itämerenkatu 11-13 , FI-00180 Helsinki , Finnland E-Mail : legal-requests@supercell.com DATEN , DIE VON UNS ERHOBEN WERDEN Daten , die Sie uns zur Verfügung stellen .
Kontaktinformationen -LRB- wie Ihren Name und Ihre E-Mail-Adresse -RRB- Spielername und Passwort Profilinformationen -LRB- wie z.B. Profilbild -RRB- Ihre Nachrichten an die Dienste -LRB- wie z.B. Chat-Protokolle und Supportanfragen des Spielers -RRB- Weitere Daten , die Sie uns wissentlich übermitteln -LRB- wie z.B. Daten zur Identifizierung eines verlorenen Kontos -RRB- Automatisch von uns erhobene Daten .
Daten über Ihr Konto und Ihren Spielfortschritt Ihre IP-Adresse und Mobilgeräte-IDs -LRB- z. B. Ihre Geräte-ID , Werbe-ID , MAC-Adresse , IMEI -RRB- Daten über Ihr Gerät , z. B. Gerätebezeichnung und Betriebssystem , Browsertyp und Sprache Daten , die anhand von Cookies und ähnliche Technologien -LRB- siehe nachstehende Zusatzinformationen -RRB- erfasst werden Allgemeine Standortdaten Präzise Geopositionsdaten -LRB- GPS , mit Ihrer Zustimmung -RRB- Daten über Ihre Nutzung des Dienstes , wie zum Beispiel Spieldaten und Ihre Interaktionen mit anderen Spielern innerhalb des Dienstes In den meisten Fällen erstellen wir auch eine spezifische ID von Supercell für Sie , wenn Sie den Dienst nutzen .
Daten , die wir von unseren Partnern sammeln .
Daten , die wir erhalten , wenn Sie ein Drittanbieter-Tool mit dem Dienst verknüpfen -LRB- z. B. Facebook , WeChat oder Google -RRB- Demografische Daten -LRB- z. B. zur Bestimmung des ungefähren Standorts Ihrer IP-Adresse -RRB- Daten zur Betrugsbekämpfung -LRB- z. B. Missbrauch von Rückerstattung in Spielen oder Klickbetrug in der Werbung -RRB- Daten von Plattformen , auf denen die Spiele ausgeführt werden -LRB- z. B. zur Überprüfung der Zahlung -RRB- Daten für Werbe - und Analysezwecke , um Ihnen einen besseren Dienst anbieten zu können WARUM ERFASSEN WIR IHRE DATEN Damit der Dienst funktioniert .
Zur Erfüllung des Vertrages verarbeiten wir die notwendigen Daten zur Erstellung von Konten , um Ihnen das Spielen unserer Spiele und die Nutzung unserer Dienste zu ermöglichen Ausführung des Dienstes Überprüfung und Bestätigung von Zahlungen Bereitstellung und Erbringung der von Ihnen angeforderten Produkte und Dienste Zusendung von Korrespondenz bezüglich des Dienstes Anpassung des Dienstes an die Bedürfnisse unserer Spieler .
Für die Bereitstellung von außergewöhnlichen Diensten für unsere Spieler , haben wir ein berechtigtes Interesse daran , Daten zu erfassen und zu verarbeiten , zur Aktualisierung und Entwicklung von Spielerprofilen Entwicklung und Verbesserung des Dienstes und des Spielerlebnisses Verwaltung der Beziehung zu Ihnen Bereitstellung sozialer Funktionen als Teil des Dienstes Anpassung Ihrer Diensterfahrung Beantwortung Ihrer Kommentare und Fragen sowie zur Bereitstellung von Support für den Spieler Bereitstellung von Angeboten von Supercell innerhalb des Dienstes sowie auf anderen Websites und Diensten sowie per E-Mail Zusendung von damit verbundenen Informationen wie Aktualisierungen , Sicherheitsmeldungen und Nachrichten des Kundensupports Ermöglichung von Kommunikation mit anderen Spielern Anzeige von personalisierter Werbung .
Um Ihnen personalisierte Werbung innerhalb des Dienstes sowie in anderen Websites und Diensten -LRB- einschließlich E-Mail -RRB- anzuzeigen , haben wir ein berechtigtes Interesse an der Verarbeitung der notwendigen Daten zur Nachverfolgung der Inhalte , auf die Sie in Verbindung mit dem Dienst zugreifen , sowie Ihr Online-Verhalten Erbringung , Abstimmung und Verbesserung unserer Werbung und des Dienstes Informationen darüber , wie Sie sich von personalisierten Werbungen abmelden können , finden Sie im nachstehenden Abschnitt ` Ihre Rechte und Möglichkeiten ' .
Aufrechterhaltung eines sicheren und fairen Dienstes .
Die Gewährleistung gleicher Wettbewerbsbedingungen innerhalb des Dienstes hat für uns oberste Priorität .
Weitere Informationen zu unseren Nutzungsrichtlinien finden Sie in den Nutzungsbedingungen von Supercell .
Um den Dienst und seine sozialen Funktionen sicher und fair zu halten , Betrug zu bekämpfen und eine akzeptable Nutzung anderweitig sicherzustellen , haben wir ein berechtigtes Interesse an der Verarbeitung der notwendigen Daten zur Analyse und Überwachung der Nutzung des Dienstes und seiner sozialen Funktionen Automatischen oder manuellen Moderation von Chats Maßnahmenergreifung im Falle von betrügerischen oder sich unangemessen verhaltenden Spielern Analyse , Profilierung und Segmentierung .
In allen oben genannten Fällen und Zwecken können wir alle erfassten Daten analysieren , profilieren und segmentieren .
Mit Ihrer Zustimmung .
Mit Ihrer Zustimmung können wir Ihre Daten für zusätzliche Zwecke verarbeiten , z. B. die Verwendung Ihres GPS-Standorts , um lokale Veranstaltungen anzuzeigen .
WER IHRE DATEN EINSEHEN KANN Abgesehen von Supercell können Ihre Daten in folgenden Situationen von anderen genutzt werden : ANDERE SPIELE UND NUTZER .
Soziale Funktionen sind ein zentraler Bestandteil unserer Spiele .
Andere Spieler und Nutzer sehen beispielsweise Ihre Profildaten , In-Game-Aktivitäten und lesen die Nachrichten , die Sie gepostet haben .
PARTNER VON SUPERCELL .
Supercell hat Partner , die Dienstleistungen für uns erbringen .
Diese Partner verarbeiten Ihre Daten nur nach den Anweisungen von Supercell zur Bereitstellung des Dienstes , wie z. B. Hosting , Spielersupport , Werbung , Analyse und Betrugsprävention .
Andere Unternehmen und Behörden .
Um Betrug und illegale Aktivitäten zu bekämpfen , können wir Daten mit anderen Unternehmen und Organisationen austauschen und sie Behörden als Antwort auf rechtmäßige Anfragen zur Verfügung stellen .
Wir können Ihre Daten auch auf der Grundlage Ihrer Einwilligung , zur Einhaltung der Gesetze oder zum Schutz der Rechte , des Eigentums oder der Sicherheit von uns , unseren Spielern oder anderen Personen offenlegen .
Werbe - und Social-Media-Partner .
Der Dienst umfasst Funktionen unserer Partner , z. B. Interaktionstools für soziale Medien und In-Game-Werbung .
Eine Liste dieser Partner finden Sie unter supercell.com/en/partner-opt-out .
Diese Partner können auf Ihre Daten zugreifen und gemäß deren eigenen Datenschutzrichtlinien arbeiten .
Wir empfehlen Ihnen , deren Datenschutzrichtlinien zu lesen , um mehr über ihre Datenverarbeitungspraktiken zu erfahren .
INTERNATIONALE DATENÜBERTRAGUNG Unser Dienst ist naturgemäß global , daher können Ihre Daten weltweit übertragen werden .
Da in verschiedenen Ländern möglicherweise andere Datenschutzgesetze gelten als in Ihrem eigenen Land , ergreifen wir Maßnahmen , um sicherzustellen , dass angemessene Sicherheitsvorkehrungen zum Schutz Ihrer Daten , wie in dieser Richtlinie erläutert , getroffen werden .
Angemessene Schutzmaßnahmen , die unsere Partner verwenden können , sind unter anderem von der EU-Kommission zugelassene Standardvertragsklauseln sowie die Privacy Shield-Zertifizierung im Falle von Übertragungen in die USA .
IHRE RECHTE UND MÖGLICHKEITEN Verzicht auf Marketing-E-Mails und andere Direktmarketing-Aktivitäten .
Sie können den Erhalt von Werbemitteilungen , wie z. B. Marketing-E-Mails von uns ablehnen , indem Sie die Anweisungen in diesen Mitteilungen befolgen .
Verzicht auf gezielte Werbung .
Sie können die interessenbezogene Werbung in mobilen Apps ablehnen , indem Sie die Datenschutzeinstellungen Ihres Android - oder iOS-Geräts prüfen und die Option `` Kein Ad-Tracking '' aktivieren -LRB- Apple iOS -RRB- oder `` interessenbezogene Werbung deaktivieren '' -LRB- Android -RRB- auswählen .
Weitere Informationen finden Sie außerdem unter : supercell.com/en/partner-opt-out .
Zur Ablehnung von personalisierten In-Game-Angeboten können Sie die Optionen in den Spieleinstellungen verwenden .
Zugriff auf die personenbezogenen Daten , die wir über Sie gespeichert haben .
Wenn Sie dies wünschen , stellen wir Ihnen eine Kopie Ihrer personenbezogenen Daten in einem elektronischen Format zur Verfügung .
Ihre zusätzlichen Rechte .
Sie haben auch das Recht , Ihre Daten zu korrigieren , Ihre Daten zu löschen , Einspruch gegen die Art zu erheben , wie wir Ihre Daten verwenden oder weitergeben und Einschränkungen festzulegen , wie wir Ihre Daten verwenden oder weitergeben .
Sie können Ihre Zustimmung jederzeit widerrufen , indem Sie beispielsweise die GPS-Standortfreigabe in Ihren Mobilgeräteeinstellungen deaktivieren .
Wir werden alle Anfragen innerhalb einer angemessenen Zeitspanne bearbeiten .
Wenn Sie weiterhin Bedenken bezüglich des Datenschutzes oder der Datennutzung haben oder der Meinung sind , dass wir dies nicht angemessen bearbeitet haben , wenden Sie sich bitte an unseren unabhängigen Konfliktlösungsanbieter mit Sitz in den USA -LRB- kostenlos -RRB- .
Sie können sich bei ungeklärten Beschwerden auch an Ihre lokale Datenschutzbehörde im Europäischen Wirtschaftsraum wenden .
COOKIES UND ÄHNLICHE TECHNOLOGIEN Wie die meisten Online-Dienste verwenden auch wir und unsere Partner Cookies und ähnliche Technologien , um den Dienst bereitzustellen und zu personalisieren , den Gebrauch zu analysieren , Anzeigen zu schalten und Betrug vorzubeugen .
Sie können Cookies in Ihren Browsereinstellungen deaktivieren , möglicherweise funktionieren einige Teile des Dienstes jedoch danach nicht richtig .
WIE WIR IHRE DATEN SCHÜTZEN Sicherheitsmaßnahmen .
Um ein sicheres Spielerlebnis zu gewährleisten , entwickeln und implementieren wir kontinuierlich administrative , technische und physische Sicherheitsmaßnahmen , um Ihre Daten vor unbefugtem Zugriff oder vor Verlust , Missbrauch oder Veränderung zu schützen .
Datenaufbewahrung .
Wir speichern Ihre Daten so lange , wie Ihr Konto aktiv ist oder solange dies zur Erbringung unseres Dienstes erforderlich ist .
Wir machen beispielsweise ungenutzte Spielkonten regelmäßig unkenntlich und prüfen unnötige Daten von Zeit zu Zeit und machen diese ebenfalls unkenntlich .
Wenn Sie uns auffordern , Ihre personenbezogenen Daten zu löschen , werden wir Ihre Daten so speichern , wie dies für unsere legitimen Geschäftsinteressen erforderlich ist , z. B. zur Erfüllung unserer gesetzlichen Verpflichtungen , zur Beilegung von Streitigkeiten und zur Durchsetzung unserer Vereinbarungen .
ALTERSBESCHRÄNKUNGEN Wir erheben oder erfragen wissentlich keine personenbezogenen Daten von und richten keine interessensbezogene Werbung an Kinder und Jugendliche unter 13 Jahren , bzw .
gestatten solchen Personen nicht die Nutzung unserer Dienste .
Wenn Sie noch nicht 13 Jahre alt sind , schicken Sie uns bitte keine Daten über sich selbst , einschließlich Ihres Namens , Ihrer Adresse , Telefonnummer oder E-Mail-Adresse .
Niemand unter 13 Jahren darf uns personenbezogene Daten angeben .
Sollten wir davon Kenntnis erhalten , dass wir personenbezogene Daten eines Kindes oder Jugendlichen unter 13 Jahren erhoben haben , werden wir diese Daten schnellstmöglich löschen .
Wenn Sie der Ansicht sind , dass wir Daten von oder über ein Kind unter 13 Jahren besitzen , wenden Sie sich bitte an uns .
