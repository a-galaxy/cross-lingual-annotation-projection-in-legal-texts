Nutzervereinbarung
Mit Wirkung vom 8. Mai 2018

Unsere Nutzervereinbarung wurde aktualisiert. Klicken Sie hier, um eine �bersicht der �nderungen anzuzeigen.

LinkedIn hat es sich zur Aufgabe gemacht, Fach- und F�hrungskr�fte weltweit miteinander zu vernetzen, um ihnen zu noch mehr Produktivit�t und Erfolg zu verhelfen. Unsere Dienste sind darauf ausgerichtet, unseren Mitgliedern wirtschaftliche M�glichkeiten zu erschlie�en, indem wir Ihnen und Millionen anderen Fach- und F�hrungskr�ften die M�glichkeit geben, sich zu treffen, Ideen auszutauschen, Neues zu lernen, berufliche und gesch�ftliche Chancen zu entdecken, Mitarbeiter zu finden und Entscheidungen in einem zuverl�ssigen Netzwerk zu treffen.

Weitere Informationen dazu, wie wir Ihre Daten nutzen, finden Sie in unserem Nutzervereinbarungsvideo.Inhaltsverzeichnis:
Einleitung
Pflichten
Rechte und Einschr�nkungen
Haftungsausschluss und Haftungsbeschr�nkung
Beendigung
Anwendbares Recht und Streitschlichtung
Allgemeine Bedingungen
Was Sie auf LinkedIn tun und nicht tun d�rfen (�Do�s and Don�ts� oder Verhaltensregeln)
Beschwerden zu Inhalten
Kontaktaufnahme
1. Einleitung
1.1 Vertrag
Durch Nutzung unserer Dienste erkl�ren Sie sich mit s�mtlichen Bedingungen einverstanden. Ihre Nutzung unserer Dienste unterliegt unserer Cookie-Richtlinie und der Datenschutzrichtlinie. Darin wird beschrieben, wie wir Ihre personenbezogenen Daten erfassen, verwenden, teilen und speichern.

Sie erkl�ren Sie sich damit einverstanden, dass Sie mit LinkedIn einen rechtsverbindlichen Vertrag abschlie�en (auch wenn Sie unsere Dienste im Namen eines Unternehmens verwenden), wenn Sie sich durch Klicken auf �Jetzt anmelden�, �Mitglied von LinkedIn werden�, �Registrieren� oder �hnliches bei LinkedIn registrieren, auf unsere Dienste zugreifen oder diese (wie unten beschrieben) verwenden. Stimmen Sie diesem Vertrag (�Vertrag� oder �Nutzervereinbarung�) nicht zu, sollten Sie nicht auf �Jetzt Mitglied werden� (oder �hnliches) klicken und auf keinerlei Weise auf unsere Dienste zugreifen. Wenn Sie diesen Vertrag k�ndigen m�chten, k�nnen Sie dies jederzeit durch das Schlie�en Ihres Kontos tun und indem Sie nicht mehr auf unsere Dienste zugreifen bzw. diese nicht mehr nutzen.

Dienste
Dieser Vertrag gilt f�r LinkedIn.com, die LinkedIn Apps, SlideShare, LinkedIn Learning und andere mit LinkedIn verbundene Webseiten, Apps, Mitteilungen und sonstige Dienste, die ausdr�cklich unter diesem Vertrag angeboten werden (�Dienste�), einschlie�lich der Offsite-Datenerfassung f�r diese Dienste, wie unsere Anzeigen sowie die Plug-ins �Mit LinkedIn bewerben� und �Mit LinkedIn teilen�. Registrierte Nutzer unserer Dienste sind �Mitglieder�, nicht registrierte Nutzer sind �Besucher�. Dieser Vertrag gilt sowohl f�r Mitglieder als auch f�r Besucher.

LinkedIn
Sie schlie�en diesen Vertrag mit LinkedIn ab (auch mit �wir� oder �uns� bezeichnet).

Mit dem Begriff �designierte L�nder� beziehen wir uns auf die L�nder in der Europ�ischen Union (EU) und im Europ�ischen Wirtschaftsraum (EWR) sowie auf die Schweiz.

Wenn Sie in �designierten L�ndern� ans�ssig sind, schlie�en Sie diesen Vertrag mit LinkedIn Ireland Unlimited Company (�LinkedIn Ireland�) ab, und LinkedIn Ireland wird zum Datenverantwortlichen f�r Ihre personenbezogenen Daten, die im Zusammenhang mit unseren Diensten bereitgestellt, erhoben oder verarbeitet werden.

Wenn Sie au�erhalb der �designierten L�nder� ans�ssig sind, schlie�en Sie diesen Vertrag mit der LinkedIn Corporation (�LinkedIn Corp.�) ab, und LinkedIn Corp. wird zum Datenverantwortlichen f�r Ihre personenbezogenen Daten, die im Zusammenhang mit unseren Diensten bereitgestellt, erhoben oder verarbeitet werden.

Dieser Vertrag gilt f�r Mitglieder und Besucher.

Da Sie Besucher oder Mitglieder unserer Dienste sind, unterliegt die Erhebung, Nutzung und Freigabe Ihrer personenbezogenen Daten dieser Datenschutzrichtlinie (einschlie�lich unserer Cookie-Richtlinie und anderer Dokumente, auf die in dieser Datenschutzrichtlinie Bezug genommen wird) und deren Aktualisierungen.

1.2. Mitglieder und Besucher
Wenn Sie sich f�r die Dienste von LinkedIn registrieren und LinkedIn beitreten oder registrierter Benutzer von SlideShare werden, so sind Sie Mitglied. Wenn Sie sich nicht f�r unsere Dienste registrieren, k�nnen Sie auf bestimmte Funktionen als �Besucher� zugreifen.

1.3 �nderung
Wir k�nnen �nderungen am Vertrag vornehmen.

Dieser Vertrag, unsere Datenschutzrichtlinie sowie die Cookie-Richtlinie werden gelegentlich ge�ndert. Sollten wir wesentliche �nderungen vornehmen, benachrichtigen wir Sie �ber unsere Dienste oder auf andere Art und Weise, damit Sie Gelegenheit haben, die �nderungen zu �berpr�fen, bevor sie in Kraft treten. Wir akzeptieren, dass �nderungen nicht r�ckwirkend vorgenommen werden k�nnen. Wenn Sie mit irgendwelchen �nderungen nicht einverstanden sind, k�nnen Sie Ihr Konto schlie�en. Die weitere Nutzung unserer Dienste Ihrerseits, nachdem wir die �nderungen an diesen Bedingungen ver�ffentlicht oder Sie �ber diese benachrichtigt haben, bedeutet, dass Sie sich mit den aktualisierten Bedingungen einverstanden erkl�ren.

2. Pflichten
2.1 Zugang zu den Diensten
In diesem Vertrag sichern Sie uns u. a. Folgendes zu:

Sie sind berechtigt, diesen Vertrag abzuschlie�en und haben das erforderliche �Mindestalter� erreicht.

Die Dienste sind nicht f�r die Nutzung durch Kinder unter einem Alter von 16 Jahren vorgesehen.

Um die Dienste nutzen zu k�nnen, stimmen Sie Folgendem zu: (1) Sie haben das �Mindestalter� (wie unten dargelegt) erreicht oder sind �lter; (2) Sie unterhalten nur ein Konto auf LinkedIn (und/oder ein Konto auf SlideShare, falls zutreffend), das Sie mit Ihrem echten Namen erstellt haben; und (3) Sie sind nicht bereits von LinkedIn eingeschr�nkt, die Dienste zu verwenden. Der Erstellen eines Kontos unter Angabe falscher Informationen stellt einen Versto� gegen unsere Bestimmungen dar; dazu z�hlen auch Konten, die im Namen anderer oder f�r von Personen unter 16 Jahren er�ffnet werden.

Das �Mindestalter� ist das Alter von 16 Jahren. Erfordert das Gesetz jedoch, dass Sie �lter sein m�ssen, damit LinkedIn Ihnen die Dienste (einschlie�lich der Nutzung Ihrer Daten) rechtm��ig ohne elterliche Zustimmung zug�nglich machen kann, bedeutet �Mindestalter� das entsprechende h�here Alter.

2.2 Ihre Mitgliedschaft
Sie werden Ihr Passwort geheim halten.

Sie nutzen ein Konto nicht mit anderen gemeinsam und halten sich an unsere Bedingungen und das Gesetz.

Mitglieder sind Kontobesitzer. Sie erkl�ren sich mit Folgendem einverstanden: (1) Sie bem�hen sich, ein starkes und sicheres Passwort zu w�hlen; (2) Sie halten Ihr Passwort geheim und behandeln es vertraulich; (3) Sie �bertragen keinen Teil Ihres Kontos an andere (beispielsweise Kontakte), und (4) Sie halten sich an das Gesetz, die Verhaltensregeln (was Sie auf LinkedIn tun und was Sie nicht tun d�rfen) und die Professionelle Community-Richtlinie. Sie tragen die Verantwortung f�r s�mtliche �ber Ihr Benutzerkonto vorgenommenen Aktionen, es sei denn, Sie haben das Konto geschlossen oder einen Missbrauch gemeldet.

In Bezug auf das Verh�ltnis zwischen Ihnen und anderen Parteien (einschlie�lich Ihres Arbeitgebers) gilt, dass Ihr Konto Ihnen geh�rt. Wurden die Dienste aber von einer anderen Partei zu Ihrer Nutzung erworben (beispielsweise eine von Ihrem Arbeitgeber gekaufte Recruiter-Lizenz), hat die f�r die Dienste zahlende Partei das Recht, den Zugriff auf die Dienste sowie Ihre Nutzung solcher bezahlten Dienste zu kontrollieren, hat jedoch keinerlei Rechte im Zusammenhang mit Ihrem pers�nlichen Konto.

2.3. Zahlung
Sie kommen Ihren Zahlungsverpflichtungen nach und sind damit einverstanden, dass wir Ihre Zahlungsinformationen speichern. Sie wissen, dass Geb�hren und Steuern auf unsere Preise aufgeschlagen werden k�nnen.

Wir garantieren keine R�ckerstattung.

Wenn Sie unsere geb�hrenpflichtigen Dienste (�Premium-Dienste�) erwerben, stimme Sie den zus�tzlichen Bedingungen f�r diese geb�hrenpflichtigen Dienste zu und erkl�ren sich einverstanden, uns die entsprechenden Geb�hren und Steuern zu zahlen. Bei Nichtzahlung dieser Geb�hren werden Ihre geb�hrenpflichtigen Dienste beendet. Des Weiteren erkl�ren Sie sich mit Folgendem einverstanden:

Ihr Kauf unterliegt m�glicherweise Devisengeb�hren oder standortbedingten oder von Devisenkursen abh�ngigen Preisunterschieden.
Wir k�nnen Ihre Zahlungsmethode (z. B. Kreditkarte) speichern und belasten, und zwar auch nachdem diese abgelaufen ist, um eine Unterbrechung unserer Dienste zu vermeiden und um sie zur Zahlung anderer Dienste zu verwenden, die Sie m�glicherweise erwerben.
Wenn Sie eine Mitgliedschaft kaufen, wird Ihre Zahlungsmethode jeweils bei Beginn einer Mitgliedschaftsperiode automatisch mit den f�r diese Periode anfallenden Geb�hren und Steuern belastet. K�ndigen Sie bitte vor dem Verl�ngerungsdatum, um k�nftige Belastungen zu vermeiden. Informieren Sie sich dar�ber, wie Sie Ihre Premium-Dienste k�ndigen oder aussetzen k�nnen.
S�mtliche K�ufe von Diensten unterliegen der R�ckerstattungsrichtlinie von LinkedIn.
Wir berechnen die von Ihnen zu zahlenden Steuern auf Basis der Rechnungsdaten, die Sie uns zur Zeit des Kaufs zur Verf�gung stellen.
Eine Kopie Ihrer Rechnung erhalten Sie �ber Ihre Kontoeinstellungen auf LinkedIn unter �Transaktions�bersicht�.

2.4 Mitteilungen und Nachrichten
Sie erkl�ren sich damit einverstanden, dass wir Ihnen �ber unsere Webseiten, Apps und Kontaktinformationen Mitteilungen und Nachrichten zukommen lassen d�rfen. Sind die von Ihnen bereitgestellten Kontaktdaten nicht auf dem aktuellen Stand, kann es sein, dass Sie wichtige Mitteilungen nicht erhalten.

Sie erkl�ren sich einverstanden, dass wir Ihnen auf folgende Weise Mitteilungen und Nachrichten zukommen lassen: (1) innerhalb des Dienstes oder (2) an die von Ihnen bereitgestellte Kontaktadresse (z. B. E-Mail-Adresse, Handy-Nummer, Postanschrift). Sie erkl�ren sich einverstanden, Ihre Kontaktdaten auf dem aktuellen Stand zu halten.

Bitte �berpr�fen Sie Ihre Einstellungen, um die Nachrichten, die Sie von uns erhalten, zu steuern und begrenzen.

2.5 Teilen
Wenn Sie Informationen zu unseren Diensten teilen, k�nnen andere Personen diese Informationen sehen, kopieren und nutzen.

Unsere Dienste erm�glichen es Ihnen, Nachrichten und Informationen auf verschiedenste Weisen zu teilen, beispielsweise �ber Ihr Profil, SlideDeck, Links zu News-Artikeln, Jobanzeigen, InMails und Blogs. Informationen und Inhalte, die Sie teilen oder posten, k�nnen von anderen Mitgliedern, Besuchern oder Dritten gesehen werden (einschlie�lich au�erhalb der Dienste). Wir respektieren die von Ihnen getroffenen Entscheidungen dar�ber, wer welche Inhalte oder Informationen sehen darf (z. B. die Inhalte Ihrer Nachrichten, mit LinkedIn Kontakten geteilte Inhalte, die Sichtbarkeit Ihres Profils f�r Suchmaschinen oder ob andere �ber Aktualisierungen Ihres Profils informiert werden sollen). Bei Jobsuchaktivit�ten benachrichtigen wir Ihr Netzwerk oder die �ffentlichkeit standardm��ig nicht. Wenn Sie sich also �ber unsere Dienste um einen Job bewerben oder angeben, dass Sie sich f�r einen Job interessieren, teilen wir diese Tatsache nur mit dem Jobinserenten.

Wir sind nicht verpflichtet, Informationen oder Inhalte auf unseren Diensten zu ver�ffentlichen und k�nnen diese nach eigenem Ermessen mit oder ohne vorherige Benachrichtigung entfernen.

3. Rechte und Einschr�nkungen
3.1. Ihre Lizenz f�r LinkedIn
S�mtliche Inhalte und s�mtliches Feedback sowie personenbezogene Daten, die Sie uns zur Verf�gung stellen, sind Ihr Eigentum. Sie gew�hren uns jedoch eine nicht ausschlie�liche Lizenz f�r dieses Material.

Wer Ihre Daten und Inhalte sieht und wie diese f�r Werbezwecke genutzt werden d�rfen, ist allein Ihrer Entscheidung �berlassen, die wir respektieren.

Im Vertragsverh�ltnis zwischen Ihnen und LinkedIn sind Sie Eigent�mer der Inhalte und Informationen, die Sie an die Dienste �bermitteln oder dort ver�ffentlichen, und Sie gew�hren LinkedIn und unseren Partnerunternehmen die folgende nicht-exklusive Lizenz:

Das weltweite, �bertragbare und unterlizenzierbare Recht, Informationen und Inhalte, die Sie �ber unsere Dienste und Dienste von Dritten bereitstellen, ohne weitere Zustimmung Ihrerseits oder Mitteilung und/oder Entsch�digungszahlung an Sie oder an Dritte zu nutzen, zu kopieren, zu modifizieren, zu verteilen, zu ver�ffentlichen und zu verarbeiten. Diese Rechte sind wie folgt eingeschr�nkt:

Sie k�nnen diese Lizenz f�r spezifische Inhalte beenden, indem Sie derartige Inhalte aus den Diensten l�schen oder Ihr Konto schlie�en, wobei zu ber�cksichtigen ist, dass (a) andere Personen die auf den Diensten geteilten Inhalte m�glicherweise kopiert, ihrerseits geteilt oder gespeichert haben, und (b) dass wir eine angemessene Frist haben, diese Inhalte aus Sicherungs- und sonstigen Systemen zu entfernen.
Wir ver�ffentlichen Ihre Inhalte (einschlie�lich Sponsored Content) nicht in Anzeigen f�r die Produkte und Dienste Dritter, die sich an andere Personen richten, ohne daf�r Ihre diesbez�gliche separate Einwilligung erhalten zu haben. Wir behalten uns jedoch das Recht vor, ohne Entsch�digung an Sie oder Dritte, in der N�he Ihrer Inhalte und Informationen Anzeigen zu schalten. Wie in der Datenschutzrichtlinie vermerkt, sind Ihre sozialen Aktionen m�glicherweise sichtbar und in Werbeanzeigen enthalten.
Wir holen Ihre Genehmigung ein, wenn wir anderen Personen das Recht erteilen m�chten, Ihre Inhalte au�erhalb unserer Dienste zu ver�ffentlichen. Teilen Sie einen Beitrag jedoch ��ffentlich�, aktivieren wir eine Funktion, mit der andere Mitglieder diesen �ffentlichen Beitrag in Dienste von Drittanbietern einbetten k�nnen, und wir aktivieren die Auffindbarkeit dieser �ffentlichen Inhalte �ber Suchmaschinen-Dienste. Mehr erfahren
Obwohl wir Ihre Inhalte m�glicherweise bearbeiten und �nderungen an der Formatierung vornehmen (indem wir beispielsweise �bersetzungen anfertigen, Gr��e, Layout oder Dateityp �ndern oder Metadaten entfernen), �ndern wir die Bedeutung Ihrer Beitr�ge nicht.
Da Sie Eigent�mer Ihrer Inhalte und Informationen sind und wir nur nicht-exklusive Rechte daran haben, k�nnen Sie diese anderen verf�gbar machen, beispielsweise unter einer Creative-Commons-Lizenz.
Sie und LinkedIn vereinbaren, dass Inhalte, die personenbezogene Daten enthalten, unserer Datenschutzrichtlinie unterliegen.

Sie erkl�ren sich damit einverstanden, dass LinkedIn auf alle Informationen und personenbezogenen Daten, die Sie in �bereinstimmung mit den Bedingungen der Datenschutzrichtlinie und Ihren Optionen (einschlie�lich Einstellungen) bereitstellen, zugreifen und diese verwenden und speichern darf.

Wenn Sie Vorschl�ge oder sonstiges Feedback bez�glich unserer Dienste an LinkedIn senden, erkl�ren Sie sich damit einverstanden, dass LinkedIn dieses Feedback ohne Entsch�digung an Sie zu beliebigen Zwecken nutzen und teilen kann (aber nicht dazu verpflichtet ist).

Sie sichern zu, uns nur Informationen und Inhalte zu liefern, die Sie an uns weitergeben d�rfen. Sie sichern au�erdem zu, dass die Inhalte Ihres LinkedIn Profils den Tatsachen entsprechen.

Sie erkl�ren sich damit einverstanden, nur Inhalte und Informationen bereitzustellen, die nicht gegen das Gesetz versto�en oder die Rechte (einschlie�lich geistiger Eigentumsrechte) anderer Personen verletzen. Sie stimmen auch zu, dass Ihre Profildaten der Wahrheit entsprechen. LinkedIn kann gesetzlich dazu verpflichtet sein, in bestimmten L�ndern gewisse Informationen oder Inhalte zu entfernen.

3.2 Verf�gbarkeit der Dienste
Wir k�nnen beliebige Dienste �ndern, aussetzen oder beenden und unsere Preise k�nftig nach unserem Ermessen �ndern. Soweit das Gesetz es zul�sst, treten diese �nderungen in Kraft, sobald wir Sie davon in Kenntnis setzen.

Wir behalten uns das Recht vor, einen beliebigen Teil unserer Dienste zu �ndern oder zu beenden. Wir verpflichten uns nicht, von Ihnen ver�ffentlichte Informationen und Inhalte zu speichern oder weiterhin anzuzeigen.

LinkedIn ist kein Speicherdienst. Sie erkl�ren sich damit einverstanden, dass wir nicht verpflichtet sind, Ihnen eine Kopie von Inhalten oder Informationen, die Sie oder andere Personen bereitstellen, verf�gbar zu machen und diese zu speichern oder aufzubewahren, soweit nicht durch ein geltendes Recht vorgeschrieben und wie in unserer Datenschutzrichtlinie dargelegt.

3.3 Andere Inhalte, Webseiten und Apps
Sie nutzen die auf unseren Diensten ver�ffentlichten Inhalte und Informationen anderer Personen auf eigene Gefahr.

Dritte k�nnen ihre eigenen Produkte und Dienste �ber LinkedIn anbieten. F�r solche Aktivit�ten Dritter sind wir nicht verantwortlich.

Durch Nutzung der Dienste werden Sie m�glicherweise Inhalten oder Informationen ausgesetzt, die falsch, unvollst�ndig, versp�tet, irref�hrend, gesetzeswidrig, beleidigend oder auf sonstige Weise sch�dlich sein k�nnten. Wir pr�fen die von unseren Mitgliedern oder Dritten bereitgestellten Inhalte grunds�tzlich nicht. Sie stimmen zu, dass wir nicht f�r Inhalte oder Informationen Dritter (einschlie�lich die anderer Mitglieder) verantwortlich sind. Diesen Missbrauch unserer Dienste k�nnen wir nicht immer verhindern. Sie stimmen zu, dass wir f�r einen derartigen Missbrauch nicht verantwortlich sind. Sie sind sich auch dar�ber im Klaren, dass Sie oder Ihr Unternehmen versehentlich mit Inhalten �ber Dritte verbunden werden k�nnten, wenn wir Ihre Kontakte und Follower informieren, dass Sie oder Ihr Unternehmen in den News erw�hnt wurden. Mitglieder k�nnen zu dieser Funktion entsprechende Einstellungen vornehmen.

Es bleibt Ihnen �berlassen, auf die Apps oder Webseiten Dritter zuzugreifen, die �ber einen Link auf unseren Diensten erreichbar sind. Wenn Sie einer Webseite oder App Dritter erlauben, Sie zu authentifizieren oder sich mit Ihrem LinkedIn Konto zu verbinden, kann diese App oder Webseite auf mit Ihnen und Ihren Kontakten verbundene Informationen auf LinkedIn zugreifen. Apps und Webseiten Dritter unterliegen ihren eigenen AGB und Datenschutzrichtlinien. Sie k�nnten dem Betreiber die Nutzung Ihrer Informationen in einer Art und Weise einr�umen, die von unseren Gepflogenheiten abweicht. LinkedIn haftet nicht f�r diese anderen Webseiten und Apps, sofern nicht durch geltendes Recht in begrenztem Ma�e vorgeschrieben. Die Nutzung dieser Webseiten und Apps geschieht auf eigene Gefahr. Bitte lesen Sie dazu unsere Datenschutzrichtlinie.

3.4 Einschr�nkungen
Wir sind berechtigt, Ihre Interaktionen und Kontakte �ber unsere Dienste einzuschr�nken.

LinkedIn beh�lt sich das Recht vor, Ihre Nutzung der Dienste, einschlie�lich der Anzahl der Kontakte und der F�higkeit, mit anderen Mitgliedern Kontakt aufzunehmen, einzuschr�nken. LinkedIn beh�lt sich das Recht vor, Ihr Konto einzuschr�nken, auszusetzen oder zu schlie�en, wenn wir der Meinung sind, dass Sie gegen diesen Vertrag oder das Gesetz versto�en oder die Dienste missbrauchen (z. B durch einen Versto� gegen die Verhaltensregeln oder die Professionelle Community-Richtlinie).

3.5 Geistige Eigentumsrechte
Wir weisen Sie auf unsere geistigen Eigentumsrechte hin.

LinkedIn beh�lt sich s�mtliche geistigen Eigentumsrechte an den Diensten vor. Nutzung der Dienste bedeutet nicht, dass Sie irgendwelche Eigentumsrechte bzgl. unserer Dienste oder der durch unsere Dienste verf�gbar gemachten Inhalte oder Informationen haben. Die in Verbindung mit den Diensten verwendeten Marken und Logos sind Marken der betreffenden Eigent�mer. Die Marken LinkedIn, SlideShare und die Logos �In� und andere Marken, Dienstleistungsmarken, Grafiken und Logos von LinkedIn, die f�r unsere Dienste verwendet werden, sind Marken oder eingetragene Marken von LinkedIn.

3.6 Automatische Verarbeitung
Wir nutzen Daten und Informationen �ber Sie, um Ihnen und anderen relevante Vorschl�ge zu unterbreiten.

Wir nutzen die von Ihnen und (anderen) Mitgliedern bereitgestellten Informationen und Daten, um Empfehlungen zu f�r Sie m�glicherweise n�tzlichen Kontakten, Inhalten und Funktionen bereitzustellen. Wir nutzen beispielsweise Daten und Informationen �ber Sie, um Ihnen Jobs zu empfehlen oder Sie Recruitern zu empfehlen. Mit einem aktuellen und korrekten Profil unterst�tzen Sie uns dabei, diese Empfehlungen relevant und zutreffend zu gestalten.

4. Haftungsausschluss und Haftungsbeschr�nkung
4.1 Gew�hrleistungsausschluss
Hiermit erkl�ren wir uns nicht haftbar f�r Qualit�t, Sicherheit oder Zuverl�ssigkeit unserer Dienste.

SOWEIT GESETZLICH ZUL�SSIG LEHNEN LINKEDIN UND SEINE VERBUNDENEN UNTERNEHMEN (UND DIEJENIGEN, MIT DENEN LINKEDIN ZUR BEREITSTELLUNG DER DIENSTE ZUSAMMENARBEITET) (A) JEDWEDE KONKLUDENTE GEW�HRLEISTUNG UND ZUSICHERUNG (Z. B. GEW�HRLEISTUNG DER MARKTG�NGIGKEIT, EIGNUNG F�R EINEN BESTIMMTEN ZWECK, RICHTIGKEIT DER DATEN UND NICHTVERLETZUNG VON RECHTEN) AB, UND (B) GEW�HRLEISTEN NICHT, DASS DIE DIENSTE OHNE UNTERBRECHUNG ODER FEHLER FUNKTIONIEREN, UND (C) STELLEN DIE DIENSTE (EINSCHLIESSLICH DER INHALTE UND INFORMATIONEN) �IM VORLIEGENDEN ZUSTAND� UND �WIE VERF�GBAR� BEREIT.

EINIGE GESETZE ERLAUBEN GEWISSE GEW�HRLEISTUNGSAUSSCHL�SSE NICHT. DAHER GELTEN EINIGE ODER ALLE DIESER GEW�HRLEISTUNGSAUSSCHL�SSE M�GLICHERWEISE NICHT F�R SIE.

4.2 Haftungsausschluss
Dies sind die Haftungseinschr�nkungen, die m�glicherweise Ihnen gegen�ber gelten.

SOWEIT GESETZLICH ZUL�SSIG (UND WENN LINKEDIN NICHT EINE SEPARATE SCHRIFTLICHE VEREINBARUNG EINGEGANGEN IST, DIE DIESEN VERTRAG ERSETZT), LEHNEN LINKEDIN UND SEINE VERBUNDENEN UNTERNEHMEN (UND DIEJENIGEN, MIT DENEN LINKEDIN ZUR BEREITSTELLUNG DER DIENSTE ZUSAMMENARBEITET) JEDWEDE HAFTUNG IHNEN ODER ANDEREN PERSONEN GEGEN�BER F�R MITTELBARE, NEBEN- UND FOLGESCH�DEN ODER STRAFSCHADENSERSATZ ODER F�R DEN VERLUST VON DATEN, CHANCEN, RUF, UMSATZ ODER GEWINN IN VERBINDUNG MIT DEN DIENSTEN AB (BEISPIELSWEISE ANST�SSIGE ODER BELEIDIGENDE �USSERUNGEN, AUSFALLZEITEN ODER VERLUST ODER NUTZUNG IHRER INFORMATIONEN ODER INHALTE BZW. �NDERUNGEN DARAN).

AUF KEINEN FALL �BERSTEIGT DIE HAFTUNG VON LINKEDIN UND SEINEN VERBUNDENEN UNTERNEHMEN (UND DENJENIGEN, MIT DENEN LINKEDIN ZUR BEREITSTELLUNG DER DIENSTE ZUSAMMENARBEITET) EINEN ALLE SCH�DEN ZUSAMMENFASSENDEN BETRAG, DER DEN VON IHNEN (A) ZULETZT MONATLICH ODER J�HRLICH F�R EINEN PREMIUM-DIENST GEZAHLTEN BETRAG UM DAS F�NFFACHE �BERSTEIGT ODER (B) DEN BETRAG VON 1.000 USD, JE NACHDEM, WELCHER BETRAG GERINGER IST.

DIESE EINSCHR�NKUNG DER HAFTUNG IST TEIL DER GRUNDLAGE DES GESCH�FTS ZWISCHEN IHNEN UND LINKEDIN UND GILT F�R ALLE HAFTUNGSANSPR�CHE (WIE GEW�HRLEISTUNG, UNERLAUBTE HANDLUNG, FAHRL�SSIGKEIT, VERTRAG ODER RECHT) UND GILT AUCH DANN, WENN LINKEDIN ODER SEINE VERBUNDENEN UNTERNEHMEN VON DER M�GLICHKEIT EINES SOLCHEN SCHADENS UNTERRICHTET WURDEN UND WENN DIESE RECHTSMITTEL IHREN EIGENTLICHEN ZWECK NICHT ERF�LLEN.

EINIGE GESETZE ERLAUBEN DIE HAFTUNGSEINSCHR�NKUNG ODER DEN HAFTUNGSAUSSCHLUSS NICHT. DAHER TREFFEN DIESE EINSCHR�NKUNGEN M�GLICHERWEISE NICHT AUF SIE ZU.

5. Beendigung
Wir k�nnen diesen Vertrag jederzeit beenden.

Sowohl Sie als auch LinkedIn k�nnen diesen Vertrag jederzeit durch Benachrichtigung der anderen Partei beenden. Bei Beendigung verlieren Sie das Recht, auf die Dienste zuzugreifen oder sie zu verwenden. Nach Beendigung gilt weiterhin Folgendes:

Wir sind berechtigt, Ihr Feedback zu nutzen und offenzulegen.
Mitglieder und/oder Besucher haben das Recht, anderen Personen Inhalte und Informationen weiterzuleiten, die Sie �ber den Dienst geteilt haben, sofern diese vor der Beendigung kopiert oder weitergeleitet wurden.
Abschnitte 4, 6, 7 und 8.2 dieses Vertrags.
Alle Betr�ge, die eine der Parteien vor der Beendigung der anderen schuldet, bleiben auch nach Beendigung geschuldet.
Sie k�nnen unseren Hilfebereich besuchen, um Ihr Konto zu schlie�en.

6. Anwendbares Recht und Streitschlichtung
F�r den unwahrscheinlichen Fall, dass ein Rechtsstreit stattfinden sollte, erkl�ren LinkedIn und Sie sich einverstanden, diesen in den Gerichten von Kalifornien, USA, unter Anwendung kalifornischer Gesetze oder in den Gerichten von Dublin, Irland, unter Anwendung irischer Gesetze auszutragen.

Leben Sie in den designierten L�ndern: Sie und LinkedIn Ireland best�tigen, dass die Gesetze von Irland, mit Ausnahme kollisionsrechtlicher Bestimmungen, f�r alle Streitf�lle gelten, die diesen Vertrag und/oder die Dienste betreffen. Sie und LinkedIn Ireland best�tigen, dass Anspr�che und Streitf�lle nur in Dublin, Irland, im Prozesswege verfolgt werden k�nnen, und beide Parteien stimmen der personenbezogenen Zust�ndigkeit der Gerichte von Dublin, Irland, zu.

Alle anderen Personen au�erhalb der designierten L�nder, einschlie�lich solcher, die au�erhalb der USA leben: Sie und LinkedIn vereinbaren, dass ausschlie�lich die Gesetze des Bundesstaates Kalifornien, USA, mit Ausnahme seiner kollisionsrechtlichen Bestimmungen, f�r alle Streitf�lle bez�glich dieses Vertrags und/oder der Dienste gelten. Sie und LinkedIn vereinbaren, dass Anspr�che und Streitf�lle nur in den Bundes- und Landesgerichten in Santa Clara County, Kalifornien, USA, im Prozesswege verfolgt werden k�nnen, und Sie und LinkedIn stimmen der personenbezogenen Zust�ndigkeit dieser Gerichte zu.

7. Allgemeine Bedingungen
Hier finden Sie wichtige Informationen zum Vertrag.

Entscheidet ein Gericht mit Amtsbefugnis �ber diesen Vertrag, dass ein Teil des Vertrags nicht durchsetzbar ist, geben wir und Sie dem Gericht unser Einverst�ndnis, die Bedingungen so zu �ndern, dass der betreffende Teil durchsetzbar ist, wobei die Absicht unver�ndert bleibt. Ist dies dem Gericht nicht m�glich, erkl�ren sich beide Parteien einverstanden, das Gericht aufzufordern, den undurchsetzbaren Teil zu entfernen und den Rest dieses Vertrags weiterhin durchzusetzen.

Soweit gesetzlich zul�ssig, ist die englische Version dieses Vertrags verbindlich. �bersetzungen sollen lediglich der besseren Lesbarkeit dienen. Dieser Vertrag (einschlie�lich zus�tzlicher Bedingungen, die wir Ihnen bekannt geben, wenn Sie eine bestimmte Funktion der Dienste verwenden), ist die einzige Vereinbarung zwischen uns bez�glich der Dienste und ersetzt alle vorherigen Vereinbarungen zu den Diensten.

Wenn wir nichts unternehmen, um einem Versto� gegen diesen Vertrag entgegenzuwirken, bedeutet das nicht, dass LinkedIn sein Recht auf die Durchsetzung dieses Vertrags aufgegeben hat. Es ist Ihnen nicht gestattet, diesen Vertrag (oder Ihre Mitgliedschaft oder die Nutzung der Dienste) ohne unsere Genehmigung an irgendwelche Personen zu �bertragen. Sie best�tigen jedoch, dass LinkedIn diesen Vertrag an seine verbundenen Unternehmen oder an eine Partei, die es kauft, ohne Ihre Genehmigung �bertragen kann. In diesem Vertrag gibt es keinerlei beg�nstigte Dritte.

Sie best�tigen, dass Sie rechtliche Benachrichtigungen an uns nur an die unter Abschnitt 10 genannten Adressen senden werden.

8. Was Sie auf LinkedIn tun und nicht tun d�rfen (�Do�s and Don�ts� oder Verhaltensregeln)
8.1 Was Sie tun d�rfen
LinkedIn ist eine Community f�r Fach- und F�hrungskr�fte. Diese Verhaltensregeln sowie die Professionelle Community-Richtlinie beschr�nken, was Sie innerhalb unserer Dienste tun d�rfen und was nicht.

Sie stimmen folgenden Aussagen zu:

Sie halten alle anwendbaren Gesetze ein, einschlie�lich, und ohne Einschr�nkung, Datenschutzrechte, geistige Eigentumsrechte, Anti-Spam-Gesetze, Ausfuhrbestimmungen, Steuergesetze und aufsichtsrechtliche Anforderungen.
Sie liefern uns korrekte Informationen und sorgen daf�r, dass sich diese immer auf dem aktuellen Stand befinden.
Sie verwenden in Ihrem Profil Ihren richtigen Namen.
Sie nutzen die Dienste auf professionelle Art und Weise.
8.2 Was Sie nicht tun d�rfen
Sie stimmen zu, dass Sie Folgendes unterlassen werden:

Eine falsche Identit�t auf LinkedIn erstellen, Ihre Identit�t falsch darstellen, ein Mitgliederprofil f�r jemand anderen au�er sich selbst (eine nat�rliche Person) erstellen oder das Konto einer anderen Person nutzen oder dies versuchen;
Software, Ger�te, Skripts, Roboter oder sonstige Mittel oder Prozesse entwickeln, unterst�tzen oder nutzen (einschlie�lich Crawler, Browser-Plug-ins und Add-ons sowie andere Techniken), um die Dienste zu scrapen oder anderweitig Profile und andere Daten von den Diensten zu kopieren;
Sicherheitsfunktionen �bergehen oder andere Zugangskontrollen oder Nutzungsbeschr�nkungen �berwinden oder umgehen (wie z. B. Gro�buchstaben bei Suchanfragen oder Profilansichen);
Direkt oder �ber Dritte (wie Suchmaschinen) durch die Dienste erlangten Informationen ohne die Genehmigung von LinkedIn kopieren, nutzen, offenlegen oder verteilen;
Informationen offenlegen, wenn Sie nicht die Genehmigung dazu haben (beispielsweise vertrauliche Informationen anderer, darunter Ihr Arbeitgeber);
Geistige Eigentumsrechte anderer verletzen, einschlie�lich Urheberrechte, Patente, Markenzeichen, Gesch�ftsgeheimnisse und sonstige Eigentumsrechte. Kopieren oder verteilen Sie beispielsweise nicht Beitr�ge oder sonstige Inhalte anderer ohne deren Genehmigung (au�er �ber die verf�gbare Teilen-Funktion), es sei denn, diese geben die Berechtigung dazu, indem sie unter einer Creative-Commons-Lizenz ver�ffentlichen;
Gegen die Immaterialg�ter- oder sonstigen Rechte von LinkedIn versto�en, einschlie�lich, aber nicht beschr�nkt auf (i) das Kopieren oder Verteilen unserer Lernvideos oder sonstiger Materialien, oder (ii) das Kopieren oder Verteilen unserer Technologie, es sei denn, diese wurde unter einer Open-Source-Lizenz freigegeben, oder (iii) die Verwendung des Wortes �LinkedIn� oder unserer Logos in einem Unternehmensnamen, einer E-Mail oder URL, au�er wie unter der Markenrichtlinie vorgesehen;
Etwas ver�ffentlichen, das Softwareviren, -w�rmer oder sonstigen sch�dlichen Code enth�lt;
Reverse Engineering, Dekompilieren, Disassemblieren, Entschl�sseln ausf�hren oder einen sonstigen Versuch unternehmen, den Quellcode der Dienste oder einer verbundenen Technologie abzuleiten, bei der es sich nicht um Open Source handelt;
Ohne unsere ausdr�ckliche Genehmigung den Eindruck erwecken, dass Sie mit LinkedIn verbunden oder von LinkedIn empfohlen sind (beispielsweise sich als Trainer von LinkedIn ausgeben);
Die Dienste oder zugeh�rige Daten vermieten, leasen, verleihen, mit ihnen handeln, sie verkaufen/weiterverkaufen oder sie auf andere Weise zu monetarisieren oder sich ohne Zustimmung von LinkedIn Zugang zu den Diensten oder damit verbundenen Daten verschaffen;
Deep-Links zu unseren Diensten ohne die Genehmigung von LinkedIn zu anderen Zwecken als zur Bekanntmachung Ihres Profils oder zur Werbung f�r eine Gruppe auf unseren Diensten erstellen;
Bots oder sonstige automatisierte Methoden verwenden, um auf die Dienste zuzugreifen, Kontakte hinzuzuf�gen oder herunterzuladen und Nachrichten zu senden oder umzuleiten;
Die Verf�gbarkeit, Leistung und Funktionalit�t der Dienste zu Konkurrenzzwecken �berwachen;
�Framing�, �Mirroring� oder eine sonstige Simulierung des Erscheinungsbilds oder der Funktionsweise der Dienste betreiben;
Die Dienste oder deren Erscheinungsbild verdecken oder auf andere Weise modifizieren (wie z. B. durch Hinzuf�gen von Elementen zu Diensten oder Entfernen, Verdecken oder Verbergen einer in den Diensten vorhandenen Werbung);
Den Betrieb der Dienste behindern oder sie unangemessen belasten (beispielsweise durch Spam, Denial-of-Service-Angriffe, Viren, Spiel-Algorithmen); und/oder
Gegen die Professionelle Community-Richtlinie oder weitere Bedingungen bez�glich eines bestimmten Dienstes versto�en, der bereitgestellt wird, wenn Sie sich f�r einen derartigen Dienst registrieren oder mit der Nutzung beginnen.
9. Beschwerden zu Inhalten
Kontaktdaten f�r Beschwerden zu von unseren Mitgliedern bereitgestellten Inhalten.

Wir nehmen R�cksicht auf die geistigen Eigentumsrechte anderer. Dementsprechend erfordert dieser Vertrag, dass von Mitgliedern ver�ffentlichte Informationen korrekt sind und nicht die geistigen Eigentumsrechte oder andere Rechte Dritter verletzen. Wir stellen eine Richtlinie und ein Verfahren f�r Beschwerden �ber von unseren Mitgliedern ver�ffentlichte Inhalte zur Verf�gung.

10. Kontaktaufnahme
Unsere Kontaktdaten. Unser Hilfebereich enth�lt au�erdem Informationen zu unseren Diensten.

F�r Nachrichten oder Klagezustellungen kontaktieren Sie uns bitte:

ONLINE ODER PER POST
